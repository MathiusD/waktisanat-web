from models import User, Avatar
from own import own_sqlite
from own.own_sqlite.models import ModelData
from cdnwakfu import Text, RecipeCategorie, RecipePattern
from config.settings import DEFAULT_LANG, DEFAULT_NOTIFY, DB, DISCORD_TOKEN
from data.wakdata import latest
import discord_api
from utils import prepAvatar

def updateLangOfUser(user:ModelData, lang:str, path:str = DB):
    db = own_sqlite.connect(path)
    users = User(db)
    return users.updateUserLang(user.discord_id, lang)

def updatePrefixOfUser(user:ModelData, prefix:str, path:str = DB):
    db = own_sqlite.connect(path)
    users = User(db)
    return users.updateUserPrefix(user.discord_id, prefix)

def prepUser(user:ModelData):
    if user.avatars:
        for avatar in user.avatars:
            avatar = prepAvatar(avatar)
    return user

def getUserId(id:int, path:str = DB):
    db = own_sqlite.connect(path)
    users = User(db)
    usr = users.fetchUserByDiscordId(id)
    if usr:
        return usr.id
    return None
    
def createUser(id:int, username:str, lang:str, path:str = DB):
    db = own_sqlite.connect(path)
    users = User(db)
    if not lang in Text.shortLangs():
        lang = DEFAULT_LANG
    user = users.addUser(username, id, lang, DEFAULT_NOTIFY)
    if user:
        return user.id
    return None

def getUser(id:int, path:str = DB, with_avatar:bool = True):
    db = own_sqlite.connect(path)
    users = User(db)
    avatars = Avatar(db, users)
    user = users.fetchUserByDiscordId(id)
    if user and with_avatar:
        user.avatars = avatars.fetchAvatarsByDiscordId(id)
        user.discord_data = discord_api.getUser(id, DISCORD_TOKEN)
    return user

def getUserByID(id:int, path:str = DB, with_avatar:bool = True):
    db = own_sqlite.connect(path)
    users = User(db)
    avatars = Avatar(db, users)
    user = users.fetchUserById(id)
    if user and with_avatar:
        user.avatars = avatars.fetchAvatarsByUserId(id)
        user.discord_data = discord_api.getUser(user.discord_id, DISCORD_TOKEN)
    return user