from cdnwakfu import Server
from own.own_sqlite.models import ModelData
from models import User, Avatar
from config.settings import DB
from own import own_sqlite

def prepAvatar(avatar:ModelData):
    avatar.server = Server.extractServerName(avatar.server)
    return avatar

def getAvatar(id:int, path:str = DB):
    db = own_sqlite.connect(path)
    users = User(db)
    avatars = Avatar(db, users)
    avatar = avatars.fetchAvatarsById(id)
    return avatar

def _joinAvts(old:list, new:list):
    if len(old) > 0:
        temp = []
        for avatar in new:
            for other in old:
                if other.id == avatar.id:
                    temp.append(avatar)
        avt = temp
    else:
        avt = new
    return avt

def directory(server:int = None, name:str = None, job:int = None, jobLvl:int = None, path:str = DB):
    avt = []
    db = own_sqlite.connect(path)
    users = User(db)
    avatars = Avatar(db, users)
    if server is None and name is None and job is None:
        return avatars.fetchAll()
    if server is not None:
        serv = Server.extractServer(server)
        avt = avatars.fetchAvatarsByServerElement(serv)
    if name is not None and len(name) > 0:
        temp = avatars.fetchAvatarsByName(name)
        avt = _joinAvts(avt, temp)
    if job is not None:
        temp = avatars.fetchAvatarsByJobAndLvl(job, jobLvl) if jobLvl is not None else avatars.fetchAvatarsByJob(job)
        avt = _joinAvts(avt, temp)
    return avt

def addAvatar(userId:int, name:str, server:int, lvl:int = 0, path:str = DB):
    if 0 <= lvl <= 215:
        db = own_sqlite.connect(path)
        users = User(db)
        avatars = Avatar(db, users)
        user = users.fetchUserById(userId)
        avatar = avatars.addAvatar(user.discord_id, name, server, lvl)
        if avatar:
            return avatars.fetchAvatarsByDiscordIdAndName(user.discord_id, name)
    return None

def editAvatar(userId:int, avatar:ModelData, path:str = DB):
    if 0 <= avatar.lvl <= 215:
        db = own_sqlite.connect(path)
        users = User(db)
        avatars = Avatar(db, users)
        user = users.fetchUserById(userId)
        return avatars.setLvl(user.discord_id, avatar.name, avatar.lvl)
    return None

def updateJobAvatar(userId:int, avatar:ModelData, jobId:int, lvl:int, path:str = DB):
    if 0 <= lvl <= 150:
        db = own_sqlite.connect(path)
        users = User(db)
        avatars = Avatar(db, users)
        user = users.fetchUserById(userId)
        return avatars.setJob(user.discord_id, avatar.name, jobId, lvl)

def removeAvatar(userId:int, avatar:ModelData, path:str = DB):
    db = own_sqlite.connect(path)
    users = User(db)
    avatars = Avatar(db, users)
    user = users.fetchUserById(userId)
    return avatars.removeAvatar(user.discord_id, avatar.name)

def getAvatarByUserAndName(userId:int, name:str, path:str = DB):
    db = own_sqlite.connect(path)
    users = User(db)
    avatars = Avatar(db, users)
    user = users.fetchUserById(userId)
    return avatars.fetchAvatarsByDiscordIdAndName(user.discord_id, name)