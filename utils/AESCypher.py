import base64
import hashlib
from Crypto import Random
from Crypto.Cipher import AES
from config.settings import AES_BLOCK_SIZE


class AESCipher(object):
    def __init__(self, key:str):
        self.cipher = AES.new(key, AES.MODE_ECB)

    def encrypt(self, raw:str):
        raw = self._pad(raw)
        encrypted = self.cipher.encrypt(raw)
        encoded = base64.b64encode(encrypted)
        return str(encoded, 'utf-8')

    def decrypt(self, raw:str):
        decoded = base64.b64decode(raw)
        decrypted = self.cipher.decrypt(decoded)
        return str(self._unpad(decrypted), 'utf-8')

    def _pad(self, s:str):
        return s + (AES_BLOCK_SIZE - len(s) % AES_BLOCK_SIZE) * chr(AES_BLOCK_SIZE - len(s) % AES_BLOCK_SIZE)

    def _unpad(self, s:str):
        return s[:-ord(s[len(s)-1:])]
