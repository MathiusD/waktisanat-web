from .cookies import Cookie
from .user import getUserByID
from config.settings import DEFAULT_LANG
from config.constants import LANG_COOKIE
from cdnwakfu import Text

def getLang():
    id = Cookie.Connected()
    if id is not None:
        user = getUserByID(id)
        if user:
            return user.lang
    else:
        lang = Cookie.get_cookie(LANG_COOKIE)
        if lang in Text.shortLangs():
            return lang
    return DEFAULT_LANG