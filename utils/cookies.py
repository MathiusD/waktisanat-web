from flask import request
from .AESCypher import AESCipher
from config.settings import AES_COOKIE_KEY, COOKIE_NAME_ID, COOKIE_NAME_KEY, COOKIE_SALT, COOKIE_LIFETIME, DB
from werkzeug.wrappers.response import Response
from .user import getUserByID
import base64

class Cookie:

    @staticmethod
    def set_cookie(response:Response, name:str, value:object, max_age:int=COOKIE_LIFETIME):
        response.set_cookie(name, value, max_age)

    @staticmethod
    def get_cookie(name:str, default:object=None):
        return request.cookies.get(name, default)

    @classmethod
    def delete_cookie(cls, response:Response, name:str):
        cls.set_cookie(response, name, "", 0)

    @classmethod
    def set_(cls, response:Response, token:str):
        cipher = AESCipher(AES_COOKIE_KEY)
        cls.set_cookie(response, COOKIE_NAME_ID, cipher.encrypt(token).encode('utf-8'))

    @classmethod
    def delete_(cls, response:Response):
        cls.delete_cookie(response, COOKIE_NAME_ID)

    @classmethod
    def get_decrypted_data(cls):
        token = None
        cipher = AESCipher(AES_COOKIE_KEY)
        #In the method decrypt, we past the value of the cookie named by the COOKIE_NAME_ID
        cookie = cls.get_cookie(COOKIE_NAME_ID)
        if cookie:
            try:
                token = cipher.decrypt(cookie)
            except:
                pass
        return token

    @classmethod
    def Connected(cls, path:str = DB):
        data = cls.get_decrypted_data()
        if data:
            user = getUserByID(int(data), path)
            if user:
                return user.id
        return None