default_target: deploy

start_debug:
	@export FLASK_ENV=development
	@flask run -p 8000 -h 0.0.0.0

start:
	@echo "WebSite Launch"
	@flask run -p 8000 -h 0.0.0.0

launch: fetch start

deploy: rebuild launch

build:
	@echo "Fetch scripts"
	@pip install --upgrade pip
	@pip install -r requirements.txt
	@echo "Fetch scripts"
	@git clone https://gitlab.com/MathiusD/waktisanat-script --depth 1
	@mv ./waktisanat-script/*.sh .
	@rm -rf waktisanat-script
	@git clone https://github.com/StartBootstrap/startbootstrap-freelancer assets/scss/freelancer --depth 1
	@git clone https://github.com/twbs/bootstrap assets/scss/freelancer/src/scss/bootstrap --depth 1
	@sh build.sh

fetch:
	@sh fetch.sh

clear_log:
	@rm logs/*.log

clear:
	@echo "Clear Repo"
	@rm -rf cdnwakfu config_wakdata fetch.py data/wakdata building own own_install.py models build.sh fetch.sh config/settings_wakdata.py data/__init__.py assets/scss/freelancer

rebuild: clear update build

rebuild_data : rebuild fetch

update:
	@git pull

docker_update: update
	@docker-compose build

docker_force_update: update
	@docker-compose build --no-cache

docker_up: docker_update docker_start

docker_start:
	@docker-compose up -d

docker_down:
	@docker-compose down

docker_restart: docker_down docker_start

docker_reload: docker_force_update docker_restart