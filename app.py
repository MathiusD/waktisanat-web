from flask import Flask, redirect, render_template, request, make_response
from config.settings import DISCORD_CLIENT_ID, BOT_PERMISSIONS, LOG, DB, HOST
from config.constants import LANG_COOKIE
from models import Guild, User, Avatar, Request, Issue
from own import own_sqlite
from http import HTTPStatus
from werkzeug.exceptions import HTTPException
from cdnwakfu import Text, Item, JobItem, Recipe
from data.wakdata import latest
import logging, time, discord_api, utils, constants
from flask_assets import Bundle, Environment

logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s', filename='logs/%s.log'%(str(time.time())), level=LOG)

logging.info("Verification Database")
if (own_sqlite.bdd_exist(DB) != True):
    logging.info("Creating Database")
    own_sqlite.bdd_create(DB)

logging.info("Connecting Database")
bdd = own_sqlite.connect(DB)

guilds = Guild(bdd)
users = User(bdd)
avatars = Avatar(bdd, users)
requests = Request(bdd, avatars)
issues = Issue(bdd, users)

app = Flask(__name__)
assets = Environment(app)
css = Bundle("../assets/scss/style.scss", filters="libsass", output="gen/style.css")
assets.register("scss_all", css)

from views import blueprints
for blueprint in blueprints:
    app.register_blueprint(blueprint)

@app.errorhandler(HTTPException)
def page_not_found(error:HTTPException):
    response = make_response(render_template('http_error.jinja', connected=utils.Cookie.Connected(), description=error.description, lang=utils.getLang(), location=utils.getLocation(), langs=Text.shortLangs(), httpCode=error.code, httpError=error.name, constants=constants))
    response.headers["X-Robots-Tag"] = "noindex"
    return response, error.code

@app.route('/')
def index():
    return render_template("home.jinja", connected=utils.Cookie.Connected(), lang=utils.getLang(), location=utils.getLocation(), langs=Text.shortLangs(), constants=constants)

@app.route("/invite")
def invite():
    uri = discord_api.inviteBotLink(DISCORD_CLIENT_ID, BOT_PERMISSIONS)
    response = redirect(uri)
    response.headers["X-Robots-Tag"] = "noindex"
    return response

@app.route("/langCallBack")
def langCallBack():
    target = request.args.get('target')
    lang = request.args.get('lang')
    if lang and lang in Text.shortLangs():
        id = utils.Cookie.Connected()
        response = redirect(target) if target else redirect("/")
        if id is None:
            utils.Cookie.set_cookie(response, LANG_COOKIE, lang)
        else:
            user = utils.getUserByID(id, with_avatar=False)
            utils.updateLangOfUser(user, lang)
        response.headers["X-Robots-Tag"] = "noindex"
        return response
    response = redirect("/")
    response.headers["X-Robots-Tag"] = "noindex"
    return response

@app.route("/sitemap.xml")
def sitemap():
    db = own_sqlite.connect(DB)
    users = User(db)
    datUsers = users.fetchAll()
    avatars = Avatar(db, users)
    datAvatars = avatars.fetchAll()
    items = []
    for ite in latest["items"]:
        items.append(Item(ite))
    for ite in latest["jobsItems"]:
        items.append(JobItem(ite))
    recipes = []
    for recipe in latest["recipes"]:
        recipes.append(Recipe(recipe))
    response = make_response(render_template("sitemap.jinja", recipes=recipes, items=items, HOST=HOST, users=datUsers, avatars=datAvatars, constants=constants))
    response.headers["content-type"] = "application/xml"
    response.headers["X-Robots-Tag"] = "noindex"
    return response

@app.route("/robots.txt")
def robot():
    response = make_response(render_template("robot.jinja", HOST=HOST))
    response.headers["X-Robots-Tag"] = "noindex"
    response.headers["content-type"] = "application/txt"
    return response