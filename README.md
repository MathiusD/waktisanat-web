# WakTisanat (Site Web)

![pipeline](https://gitlab.com/MathiusD/waktisanat-web/badges/master/pipeline.svg)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/b82a52332fdd489f963c7210c5e9ba27)](https://www.codacy.com/manual/MathiusD/waktisanat-web?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=MathiusD/waktisanat-web&amp;utm_campaign=Badge_Grade)
[![Discord](https://img.shields.io/discord/722850007354703922?color=blue&label=discord&logo=discord)](https://discord.gg/Xg6uD2V)
[![ko-fi](https://www.ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/S6S023QRN)

## Concept

Ceci est la plateforme web associé au bot Waktisanat.

## Auteur

Mathieu Féry (Aka Mathius)
