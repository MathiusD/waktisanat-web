from flask import redirect, request, render_template, Blueprint
from config.settings import HOST, DISCORD_CLIENT_ID, DISCORD_CLIENT_SECRET
from config.constants import LANG_COOKIE
import discord_api, utils

auth_app = Blueprint("auth_api", __name__)

REDIRECT_URI = '%s/loginCallBack' % HOST

@auth_app.route('/login')
def login():
    uri = discord_api.redirectOAuth(DISCORD_CLIENT_ID, REDIRECT_URI)
    response = redirect(uri)
    response.headers["X-Robots-Tag"] = "noindex"
    return response

@auth_app.route('/loginCallBack')
def loginCallBack():
    code = request.args.get('code')
    if code:
        raw_user = discord_api.getUserFromOAuth(DISCORD_CLIENT_ID, DISCORD_CLIENT_SECRET, code, REDIRECT_URI)
        if raw_user:
            id = utils.getUserId(raw_user["id"])
            if not id:
                id = utils.createUser(int(raw_user["id"]), raw_user["username"], raw_user["locale"])
            if id is not None:
                response = redirect("/")
                utils.Cookie.delete_cookie(response, LANG_COOKIE)
                utils.Cookie.set_(response, str(id))
                response.headers["X-Robots-Tag"] = "noindex"
                return response
    response = redirect('/')
    response.headers["X-Robots-Tag"] = "noindex"
    return response

@auth_app.route('/logout')
def logout():
    response = redirect('/')
    utils.Cookie.delete_(response)
    response.headers["X-Robots-Tag"] = "noindex"
    return response