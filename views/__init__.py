from .auth_api import auth_app
from .profile_api import profile_app
from .avatar_api import avatar_app
from .item_api import item_app
from .recipe_api import recipe_app

blueprints = [
    auth_app,
    profile_app,
    avatar_app,
    item_app,
    recipe_app
]