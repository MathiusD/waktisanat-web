from flask import Blueprint, abort, render_template
from http import HTTPStatus
from data.wakdata import latest
from cdnwakfu import Recipe, Text, Server
from models import Avatar, User
from config.settings import DB
from own.own_sqlite import connect
import utils, constants

recipe_app = Blueprint("recipe_api", __name__)

@recipe_app.route("/recipe/<id>")
def recip(id:str):
    id = int(id) if id.isnumeric() else None
    if id:
        recipe = Recipe.findRecipe(latest["recipes"], id)
        if recipe:
            recipe.fetchData(latest)
            db = connect(DB)
            users = User(db)
            avatars = Avatar(db, users)
            servAvatars = []
            for server in Server.servers:
                if server.active:
                    servAvatars.append({
                        "server":server,
                        "avatars":avatars.extractAvatarCanCraftRecipe(recipe, server)
                    })
            return render_template("recipe.jinja", servAvatars = servAvatars, recipe=recipe, connected=utils.Cookie.Connected(), lang=utils.getLang(), location=utils.getLocation(), langs=Text.shortLangs(), constants=constants)
    abort(HTTPStatus.NOT_FOUND, description="Item Not Found")