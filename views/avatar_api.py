from flask import Blueprint, render_template, abort, request, redirect
from http import HTTPStatus
from cdnwakfu import Text, Server, RecipeCategorie
from data.wakdata import latest
import utils, constants

avatar_app = Blueprint("avatar_api", __name__)

@avatar_app.route("/avatar", methods=["GET", "POST"])
def addAvatar():
    id = utils.Cookie.Connected()
    if id is not None:
        user = utils.getUserByID(id)
        name = request.form.get('name')
        server = request.form.get('server')
        lvl = request.form.get('lvl')
        if server is not None:
            if server.isnumeric():
                server = Server.extractServer(int(server))
                if server is not None:
                    server = Server.indexOfServerElement(server)
            else:
                server = None
        if lvl is not None and lvl.isnumeric():
                lvl = int(lvl)
                if lvl < 0 or lvl > 215:
                    lvl = 0
        else:
            lvl = 0
        jobs = []
        for data in latest["recipeCategories"]:
            job = RecipeCategorie(data)
            temp = request.form.get("job-%i" % job.id)
            temp = int(temp) if temp is not None and temp.isnumeric() else None
            if temp:
                jobs.append({
                    "job":job,
                    "lvl":temp
                })
        avatar = None
        if server is not None and name is not None:
            avatar = utils.addAvatar(id, name, server, lvl)
            if avatar:
                for data in jobs:
                    utils.updateJobAvatar(id, avatar, data["job"].id, data["lvl"])
                return redirect('/avatar/%i' % utils.getAvatarByUserAndName(id, name).id)
            else:
                avatar = {
                    "name":name,
                    "server":server,
                    "lvl":lvl
                }
                for data in jobs:
                    avatar["job-%i" % data["job"].id] = data["lvl"]
        wakJobs = []
        for job in latest["recipeCategories"]:
            wakJobs.append(RecipeCategorie(job))
        return render_template("add_avatar.jinja", avatar=avatar, connected=utils.Cookie.Connected(), lang=utils.getLang(), location=utils.getLocation(), langs=Text.shortLangs(), servers=Server.servers, jobs=wakJobs, constants=constants)
    return redirect('/login')

@avatar_app.route("/avatar/<id>", methods=["GET", "POST"])
def avatar(id:int):
    id = int(id) if id.isnumeric() else None
    if id:
        avatar = utils.getAvatar(id)
        if avatar:
            lvl = request.form.get("lvl")
            lvl = int(lvl) if lvl is not None and lvl.isnumeric() else None
            jobs = []
            for data in latest["recipeCategories"]:
                job = RecipeCategorie(data)
                temp = request.form.get("job-%i" % job.id)
                temp = int(temp) if temp is not None and temp.isnumeric() else None
                if temp:
                    jobs.append({
                        "job":job,
                        "lvl":temp
                    })
            if lvl or len(jobs) > 0:
                userId = utils.Cookie.Connected()
                if userId:
                    if avatar.id_user == userId:
                        if lvl:
                            avatar.lvl = lvl
                            avatar = utils.editAvatar(userId, avatar)
                        for data in jobs:
                            utils.updateJobAvatar(userId, avatar, data["job"].id, data["lvl"])
                        avatar = utils.getAvatar(id)
                    else:
                        abort(HTTPStatus.UNAUTHORIZED, description="Isn't your avatar")
                else:
                    return redirect("/login")
            avatar = utils.prepAvatar(avatar)
            user = utils.getUserByID(avatar.id_user, with_avatar=False)
            return render_template("avatar.jinja", user=user, avatar=avatar, connected=utils.Cookie.Connected(), lang=utils.getLang(), location=utils.getLocation(), langs=Text.shortLangs(), constants=constants)
    abort(HTTPStatus.NOT_FOUND, description="Avatar doesn't exist")

@avatar_app.route("/avatar/<id>/delete", methods=["GET", "POST"])
def deleteAvatar(id:int):
    id = int(id) if id.isnumeric() else None
    if id:
        avatar = utils.getAvatar(id)
        if avatar:
            userId = utils.Cookie.Connected()
            if userId:
                if avatar.id_user == userId:
                    name = request.form.get("name")
                    if name is not None and name.lower().strip() == avatar.name.lower().strip():
                        utils.removeAvatar(userId, avatar)
                        return redirect("/profile")
                    avatar = utils.prepAvatar(avatar)
                    return render_template("remove_avatar.jinja", avatar=avatar, connected=utils.Cookie.Connected(), lang=utils.getLang(), location=utils.getLocation(), langs=Text.shortLangs(), constants=constants)
                abort(HTTPStatus.UNAUTHORIZED, description="Isn't your avatar")
            response = redirect("/login")
            response.headers["X-Robots-Tag"] = "noindex"
            return response
    abort(HTTPStatus.NOT_FOUND, description="Avatar doesn't exist")

@avatar_app.route("/directory")
def directory():
    server = request.args.get('server')
    if server is not None:
        if server.isnumeric():
            server = Server.extractServer(int(server))
            if server is not None:
                server = Server.indexOfServerElement(server)
        else:
            server = None
    name = request.args.get('name')
    job = request.args.get('job')
    job = int(job) if job is not None and job.isnumeric() else None
    jobLvl = request.args.get('jobLvl')
    jobLvl = int(jobLvl) if jobLvl is not None and jobLvl.isnumeric() else None
    avts = utils.directory(server, name, job, jobLvl)
    req = {
        "server":server,
        "name":name,
        "job":job,
        "jobLvl":jobLvl
    }
    jobs = []
    for job in latest["recipeCategories"]:
        jobs.append(RecipeCategorie(job))
    return render_template("directory.jinja", req=req, avatars=avts if avts is not None else [], connected=utils.Cookie.Connected(), lang=utils.getLang(), location=utils.getLocation(), langs=Text.shortLangs(), servers=Server.servers, jobs=jobs, constants=constants)