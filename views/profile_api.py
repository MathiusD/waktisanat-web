from flask import redirect, render_template, Blueprint, abort, request
from http import HTTPStatus
from config.settings import HOST
from cdnwakfu import Server, Text
import utils, constants

profile_app = Blueprint("profile_api", __name__)

@profile_app.route('/profile', methods=["GET", "POST"])
def profile():
    id = utils.Cookie.Connected()
    if id is not None:
        user = utils.getUserByID(id)
        lang = request.form.get('lang')
        if lang:
            if utils.updateLangOfUser(user, lang):
                user.lang = lang
        prefix = request.form.get('prefix')
        if prefix:
            if utils.updatePrefixOfUser(user, prefix):
                user.prefix = prefix
        return render_template("own_profile.jinja", user=user, avatars=user.avatars, connected=utils.Cookie.Connected(), lang=utils.getLang(), location=utils.getLocation(), langs=Text.shortLangs(), constants=constants)
    return redirect('/login')

@profile_app.route('/profile/<id>')
def profileId(id:int):
    id = int(id) if id.isnumeric() else None
    if id:
        user = utils.getUser(id)
        if user:
            if user.id == utils.Cookie.Connected():
                return redirect('/profile')
            return render_template("profile.jinja", user=user, avatars=user.avatars, connected=utils.Cookie.Connected(), lang=utils.getLang(), location=utils.getLocation(), langs=Text.shortLangs(), constants=constants)
    abort(HTTPStatus.NOT_FOUND, description="Profile doesn't exist")