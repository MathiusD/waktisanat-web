from flask import Blueprint, request, render_template, abort, redirect
from cdnwakfu import SearchItem, Text, Recipe
from data.wakdata import latest
from http import HTTPStatus
import utils, constants

item_app = Blueprint("item_api", __name__)

@item_app.route("/search")
def search():
    name = request.args.get('name')
    items = SearchItem.getItemsByName(latest, name, utils.getLang()) if name else []
    if len(items) == 1:
        return redirect("/item/%i" % items[0]._id)
    return render_template("search.jinja", items=items, connected=utils.Cookie.Connected(), lang=utils.getLang(), location=utils.getLocation(), langs=Text.shortLangs(), constants=constants)

@item_app.route("/item/<id>")
def item(id:str):
    id = int(id) if id.isnumeric() else None
    if id:
        item = SearchItem.getItem(latest, id)
        if item:
            item.fetchData(latest)
            if item.title is not None:
                recipes = Recipe.findRecipeByItemResult(latest, item)
                for recipe in recipes:
                    recipe.fetchData(latest)
                return render_template("item.jinja", item=item, recipes=recipes, connected=utils.Cookie.Connected(), lang=utils.getLang(), location=utils.getLocation(), langs=Text.shortLangs(), constants=constants)
    abort(HTTPStatus.NOT_FOUND, description="Item Not Found")