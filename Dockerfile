# Last python version with no error related to pycrypto
FROM python:3.9

COPY . /app

WORKDIR /app

RUN make rebuild_data

EXPOSE 8000

CMD ["make", "start"]