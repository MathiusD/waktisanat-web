from http import HTTPStatus
import requests
from discord_api import API_ENDPOINT

def redirectOAuth(client_id:int, redirect_uri:str):
    DISCORD_OAUTH = "https://discord.com/api/oauth2/authorize"
    uri = "%s?client_id=%i&redirect_uri=%s&response_type=code&scope=identify" % (DISCORD_OAUTH, client_id, redirect_uri)
    return uri

def getTokenFromOAuth(client_id:int, client_secret:str, code:str, redirect_uri:str):
    data = {
        'client_id': client_id,
        'client_secret': client_secret,
        'grant_type': 'authorization_code',
        'code': code,
        'redirect_uri': redirect_uri,
        'scope': 'identify'
    }
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    r = requests.post('%s/oauth2/token' % API_ENDPOINT, data=data, headers=headers)
    if r.status_code == HTTPStatus.OK:
        return r.json()
    return None

def getUserFromToken(token_type:str, access_token:str):
    data = {"Authorization": "%s %s" % (token_type, access_token)}
    r = requests.get('%s/users/@me' % API_ENDPOINT, headers=data)
    if r.status_code == HTTPStatus.OK:
        return r.json()
    return None

def getUserFromOAuth(client_id:int, client_secret:str, code:str, redirect_uri:str):
    data = getTokenFromOAuth(client_id, client_secret, code, redirect_uri)
    if data and "token_type" in data and "access_token" in data:
        return getUserFromToken(data["token_type"], data["access_token"])
    return None

def getUserAndTokenFromOAuth(client_id:int, client_secret:str, code:str, redirect_uri:str):
    data = getTokenFromOAuth(client_id, client_secret, code, redirect_uri)
    if data and "token_type" in data and "access_token" in data:
        out = {
            "token":data,
            "user":getUserFromToken(data["token_type"], data["access_token"])
        }
        return out
    return None

def inviteBotLink(client_id:int, bot_permission:int):
    DISCORD_OAUTH = "https://discord.com/oauth2/authorize"
    uri = "%s?client_id=%i&permissions=%i&scope=bot" % (DISCORD_OAUTH, client_id, bot_permission)
    return uri