from http import HTTPStatus
import requests
from discord_api import API_ENDPOINT

def getUser(userId:int, botSecret:str):
    data = {"Authorization": "Bot %s" % (botSecret)}
    r = requests.get("%s/users/%i" % (API_ENDPOINT, userId), headers=data)
    if r.status_code == HTTPStatus.OK:
        return r.json()
    return None